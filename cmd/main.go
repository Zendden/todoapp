package main

import (
	"log"

	todo "gitlab.com/Zendden/todoapp"
	"gitlab.com/Zendden/todoapp/pkg/handler"
	"gitlab.com/Zendden/todoapp/pkg/repository"
	"gitlab.com/Zendden/todoapp/pkg/service"
)

func main() {
	repos := repository.NewRepository()
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	srv := new(todo.Server)
	if err := srv.Run("8080", handlers.InitRoutes()); err != nil {
		log.Fatalf("error occured while running server: %s", err.Error())
	}
}