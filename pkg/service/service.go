package service

import "gitlab.com/Zendden/todoapp/pkg/repository"

type Auth interface {

}

type TodoList interface {

}

type TodoItem interface {

}

type Service struct {
	Auth
	TodoList
	TodoItem
}

func NewService(repo *repository.Repository) *Service {
	return &Service{}
}